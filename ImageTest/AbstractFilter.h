//
//  AbstractFilter.h
//  ImageTest
//
//  Created by Sergey Kim on 03.08.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreImage;
@import UIKit;

@interface AbstractFilter : NSObject

- (UIImage*) filteredImage:(UIImage*)sourceImage;

@end
