//
//  FLInvert.m
//  ImageTest
//
//  Created by Sergey Kim on 03.08.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import "FLInvert.h"

@implementation FLInvert

- (UIImage*) filteredImage:(UIImage*)sourceImage {
    CGRect imageRect = CGRectMake(0, 0, sourceImage.size.width, sourceImage.size.height);
    
    // Grayscale color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    
    CGContextRef context = CGBitmapContextCreate(nil, sourceImage.size.width, sourceImage.size.height, 8, 0, colorSpace, (CGBitmapInfo) kCGImageAlphaNone);
    
    CGContextDrawImage(context, imageRect, [sourceImage CGImage]);
    
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:imageRef];
    
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    CFRelease(imageRef);
    
    return newImage;
}

@end
