//
//  ImageDownloader.m
//  ImageTest
//
//  Created by Sergey Kim on 02.07.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import "ImageDownloader.h"
@import UIKit;

typedef void (^ImageDownloaderProgressBlock)(NSUInteger bytes, long long totalBytes, long long totalBytesExpected);
typedef void (^ImageDownloaderSuccessBlock)(id result);
typedef void (^ImageDownloaderErrorBlock)(NSError* error);

@interface ImageDownloader() <NSURLConnectionDelegate> {
    long expectedBytes;
}

@property (strong) NSMutableData * receivedData;

@property (copy) ImageDownloaderProgressBlock progressBlock;
@property (copy) ImageDownloaderSuccessBlock successBlock;
@property (copy) ImageDownloaderErrorBlock errorBlock;

@end


@implementation ImageDownloader

+ (id) instance {
    static ImageDownloader *instance1 = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance1 = [[self alloc] init];
    });
    return instance1;
}


- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    _receivedData = nil;
    return self;
}

- (void) startDownloadImageWithURL:(NSURL*)url
                          progress:(void (^)(NSUInteger bytes, long long totalBytes, long long totalBytesExpected))progressBlock
                           success:(void (^)(id result))successBlock
                             error:(void (^)(NSError* error))errorBlock {
    
    _progressBlock = progressBlock;
    
    _successBlock = successBlock;
    
    _errorBlock = errorBlock;
    
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:url
                                                cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                            timeoutInterval:60];
    
    _receivedData = [[NSMutableData alloc] initWithLength:0];
    NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:theRequest
                                                                   delegate:self
                                                           startImmediately:NO];
    [connection start];
}

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [_receivedData setLength:0];
    expectedBytes = [response expectedContentLength];
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [_receivedData appendData:data];
    
    if ( _progressBlock ) {
        _progressBlock( [_receivedData length], expectedBytes, expectedBytes );
    }
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if ( _errorBlock ) {
        _errorBlock (error);
        
        _progressBlock = nil;
        _errorBlock = nil;
    }
}

- (NSCachedURLResponse *) connection:(NSURLConnection *)connection
                   willCacheResponse:(NSCachedURLResponse *)cachedResponse {
    return nil;
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if ( _successBlock ) {
        _successBlock( _receivedData );
        
        _progressBlock = nil;
        _successBlock = nil;
    }
}
@end
