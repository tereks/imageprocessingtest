//
//  AppDelegate.h
//  ImageTest
//
//  Created by Sergey Kim on 01.07.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

