//
//  FLMagicalMirror.m
//  ImageTest
//
//  Created by Sergey Kim on 03.08.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import "FLMagicalMirror.h"

@implementation FLMagicalMirror

- (UIImage*) filteredImage:(UIImage*)sourceImage {
    UIImage * flippedImage = [UIImage imageWithCGImage:sourceImage.CGImage scale:1.0 orientation:UIImageOrientationUpMirrored];
    
    CGImageRef inImage = sourceImage.CGImage;
    CGContextRef context = CGBitmapContextCreate(NULL,
                                                 CGImageGetWidth(inImage),
                                                 CGImageGetHeight(inImage),
                                                 CGImageGetBitsPerComponent(inImage),
                                                 CGImageGetBytesPerRow(inImage),
                                                 CGImageGetColorSpace(inImage),
                                                 CGImageGetBitmapInfo(inImage)
                                                 );
    
    CGRect cropRect = CGRectMake(0, 0, flippedImage.size.width/2, flippedImage.size.height);
    CGImageRef rightHalf = CGImageCreateWithImageInRect(flippedImage.CGImage, cropRect);
    CGContextDrawImage(context, CGRectMake(0, 0, CGImageGetWidth(inImage), CGImageGetHeight(inImage)), inImage);
    
    CGAffineTransform transform = CGAffineTransformMakeTranslation(flippedImage.size.width, 0.0);
    transform = CGAffineTransformScale(transform, -1.0, 1.0);
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(context, cropRect, rightHalf);
    
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return finalImage;
}

@end
