//
//  ViewController.m
//  ImageTest
//
//  Created by Sergey Kim on 01.07.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import "ViewController.h"
#import "ImageViewCell.h"
#import "ProgressView.h"

#import "ImageDownloader.h"
#import "ImageProcessor.h"
#import "ImageData.h"

#import <MobileCoreServices/UTCoreTypes.h>

@interface ViewController () <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, ImageProcessingDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *sourceImageView;
@property (weak, nonatomic) IBOutlet UIButton *selectButton;

@property (assign, nonatomic) NSUInteger indexOfSelectedImage;
@property (weak, nonatomic) IBOutlet UITableView *m_tableView;
@property (weak, nonatomic) IBOutlet ProgressView *progressView;
@property (weak, nonatomic) IBOutlet UISwitch *slowProcessingSwitch;
@property (strong, nonatomic) ImageProcessor *imageProcessor;

@end

@implementation ViewController

- (void) viewDidLoad {
    [super viewDidLoad];
    _imageProcessor = [[ImageProcessor alloc] init];
    _imageProcessor.delegate  = self;
    
    _m_tableView.tableFooterView = nil;
    _progressView.alpha = 0;
}

- (void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - TableView Delegate
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _imageProcessor.images.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ImageViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"imageCell" forIndexPath:indexPath];
    
    ImageData * iData  = _imageProcessor.images[indexPath.row];
    cell.m_imageView.image = iData.image;
    [cell setProgress:iData.progress];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ImageData * iData  = _imageProcessor.images[indexPath.row];
    if ( iData.progress == 1 ) {
        _indexOfSelectedImage = indexPath.row;
        [self askUserWhatToDoWithImage];
    }
}

- (IBAction) selectImageTapped:(id)sender {

    if ([UIAlertController class])
    {
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Choose image source"
                                   message:nil
                                   preferredStyle:UIAlertControllerStyleActionSheet];
        
        __weak ViewController * myself = self;
        UIAlertAction* takePicture = [UIAlertAction actionWithTitle:@"Take picture" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action) {
                                                                [myself openImageFromTakePicture];
                                                                
                                                            }];
        UIAlertAction* cameraRoll = [UIAlertAction actionWithTitle:@"From Gallery" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action) {
                                                               [myself openImageFromCameraRoll];
                                                               
                                                           }];
        UIAlertAction* webDownload = [UIAlertAction actionWithTitle:@"From Web" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action) {
                                                                [myself openImageFromWeb];
                                                            }];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                       handler:nil];
        
        [alert addAction:takePicture];
        [alert addAction:cameraRoll];
        [alert addAction:webDownload];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        UIActionSheet *actSheet = [[UIActionSheet alloc] initWithTitle:@"Choose image source"
                                                              delegate:self
                                                     cancelButtonTitle:@"Cancel"
                                                destructiveButtonTitle:nil
                                                     otherButtonTitles:@"Take picture", @"From Gallery", @"From Web", nil];
        actSheet.tag = 200;
        [actSheet showInView:self.view];
    }
}

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if ( actionSheet.tag == 200 ) {
        switch (buttonIndex) {
            case 0:
                [self openImageFromTakePicture];
                break;
            case 1:
                [self openImageFromCameraRoll];
                break;
            default:
                [self openImageFromWeb];
                break;
        }
    }
    else {
        switch (buttonIndex) {
            case 0:
                [self saveImageToLibrary];
                break;
            case 1:
                [self removeImageFromList];
                break;
            default:
                [self replaceSourceImage];
                break;
        }
    }
}

- (void) actionSheetCancel:(UIActionSheet *)actionSheet {
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
}

- (void) openImageFromTakePicture {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = [NSArray arrayWithObjects: (NSString *) kUTTypeImage, nil];
        
        imagePicker.allowsEditing = YES;
        imagePicker.wantsFullScreenLayout = YES;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error to access Camera"
                                                        message:@""
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void) openImageFromCameraRoll {
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.wantsFullScreenLayout = YES;
        imagePicker.mediaTypes = [NSArray arrayWithObjects: (NSString *) kUTTypeImage, nil];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            [self presentViewController:imagePicker animated:YES completion:nil];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error access photo library"
                                                        message:@"your device non support photo library"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void) openImageFromWeb {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Web Image"
                                                    message:@"Enter image source url"
                                                   delegate:self
                                          cancelButtonTitle:@"Done"
                                          otherButtonTitles:nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].text = @"http://goo.gl/Ex4fqd";
    
    alert.tag = 400;
    [alert show];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString* imageUrl = [alertView textFieldAtIndex:0].text;
    __weak ProgressView * myProgress = _progressView;
    
    if ( [imageUrl length] > 0 ) {
        [_selectButton setTitle:@"" forState:UIControlStateNormal];
        
        [myProgress startAnimating];
        
        [[ImageDownloader instance] startDownloadImageWithURL:[NSURL URLWithString:imageUrl] progress:^(NSUInteger bytes, long long totalBytes, long long totalBytesExpected) {
            float progress = (float)bytes / (float)totalBytes;
            dispatch_async( dispatch_get_main_queue(), ^{
                [myProgress setProgress:progress];
            });
        } success:^(id result) {
            NSData * resultData = UIImagePNGRepresentation([UIImage imageWithData:result]);
            
            dispatch_async( dispatch_get_main_queue(), ^{
                [myProgress stopAnimating];
                _sourceImageView.image = [UIImage imageWithData:result];
            });
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsPath = [paths objectAtIndex:0];
            NSString * fileName = [NSString stringWithFormat:@"%@.png", [[NSUUID UUID] UUIDString]];
            NSString *filePath = [documentsPath stringByAppendingPathComponent:fileName];
            [resultData writeToFile:filePath atomically:YES];
            
        } error:^(NSError *error) {
            dispatch_async( dispatch_get_main_queue(), ^{
                [myProgress stopAnimating];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                message:[error localizedDescription]
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                alert.alertViewStyle = UIAlertViewStyleDefault;
                [alert show];
            });
        }];
    }
}

#pragma mark - Image Picker Delegate
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [_selectButton setTitle:@"" forState:UIControlStateNormal];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        _sourceImageView.image = [info objectForKey:UIImagePickerControllerOriginalImage];
    }];
}

#pragma mark - image processing
- (IBAction) rotateImageTapped:(id)sender {
    if ( _sourceImageView.image ) {
        [_imageProcessor addImage:_sourceImageView.image filter:ImageFilterRotate slowProcessing:_slowProcessingSwitch.on];
        [self addItemToTableView];
    }
}

- (IBAction) bwTapped:(id)sender {
    if ( _sourceImageView.image ) {
        [_imageProcessor addImage:_sourceImageView.image filter:ImageFilterBlackAndWhite slowProcessing:_slowProcessingSwitch.on];
        [self addItemToTableView];
    }
}

- (IBAction) mirrorTapped:(id)sender {
    if ( _sourceImageView.image ) {
        [_imageProcessor addImage:_sourceImageView.image filter:ImageFilterMirror slowProcessing:_slowProcessingSwitch.on];
        [self addItemToTableView];
    }
}

- (IBAction) invertColors:(id)sender {
    if ( _sourceImageView.image ) {
        [_imageProcessor addImage:_sourceImageView.image filter:ImageFilterInvertColors slowProcessing:_slowProcessingSwitch.on];
        [self addItemToTableView];
    }
}

- (IBAction) halfMirror:(id)sender {
    if ( _sourceImageView.image ) {
        [_imageProcessor addImage:_sourceImageView.image filter:ImageFilterHalfMirror slowProcessing:_slowProcessingSwitch.on];
        [self addItemToTableView];
    }
}

- (void) addItemToTableView {
    
    NSIndexPath * insertIndexPath = [NSIndexPath indexPathForRow:[_imageProcessor.images count]-1 inSection:0];
    [_m_tableView beginUpdates];
    [_m_tableView insertRowsAtIndexPaths:@[insertIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [_m_tableView endUpdates];
    
    [_m_tableView scrollToRowAtIndexPath:insertIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

- (void) askUserWhatToDoWithImage {
    if ([UIAlertController class])
    {
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Image process action"
                                   message:nil
                                   preferredStyle:UIAlertControllerStyleActionSheet];
        
        __weak ViewController * myself = self;
        UIAlertAction* takePicture = [UIAlertAction actionWithTitle:@"Save image to library" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action) {
                                                                [myself saveImageToLibrary];
                                                                
                                                            }];
        UIAlertAction* cameraRoll = [UIAlertAction actionWithTitle:@"Remove from list" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action) {
                                                               [myself removeImageFromList];
                                                               
                                                           }];
        UIAlertAction* webDownload = [UIAlertAction actionWithTitle:@"Replace source image" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action) {
                                                                [myself replaceSourceImage];
                                                            }];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction * action) {
                                                       }];
        
        [alert addAction:takePicture];
        [alert addAction:cameraRoll];
        [alert addAction:webDownload];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        UIActionSheet *actSheet = [[UIActionSheet alloc] initWithTitle:@"Image process action"
                                                              delegate:self
                                                     cancelButtonTitle:@"Cancel"
                                                destructiveButtonTitle:nil
                                                     otherButtonTitles:@"Save image to library", @"Remove from list", @"Replace source image", nil];
        actSheet.tag = 300;
        [actSheet showInView:self.view];
    }
}

- (void) saveImageToLibrary {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage * selectedImage = ((ImageData*)_imageProcessor.images[_indexOfSelectedImage]).image;
        UIImageWriteToSavedPhotosAlbum( selectedImage, nil, nil, nil);
    });
}

- (void) removeImageFromList {
    [_imageProcessor removeItemAtIndex:_indexOfSelectedImage];
    
    NSIndexPath * removeIndexPath = [NSIndexPath indexPathForRow:_indexOfSelectedImage inSection:0];
    [_m_tableView beginUpdates];
    [_m_tableView deleteRowsAtIndexPaths:@[removeIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [_m_tableView endUpdates];
}

- (void) replaceSourceImage {
    UIImage * selectedImage = ((ImageData*)_imageProcessor.images[_indexOfSelectedImage]).image;
    _sourceImageView.image = selectedImage;
}

#pragma mark - Image Rpocessor Delegate 
- (void) didConvertImageAtIndex:(NSUInteger)index {
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [_m_tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

- (void) didStepProgress:(float)progress forIndex:(NSUInteger)index {
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [_m_tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

@end
