//
//  ImageFilter.m
//  ImageTest
//
//  Created by Sergey Kim on 03.08.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import "ImageFilter.h"
#import "AbstractFilter.h"

#import "FLRotate.h"
#import "FLMirror.h"
#import "FLInvert.h"
#import "FLInvertColors.h"
#import "FLMagicalMirror.h"

@implementation ImageFilter

- (UIImage*) filteredImageFrom:(UIImage*)sourceImage withType:(ImageFilterType)filterType {
    UIImage * retImage = nil;
    AbstractFilter * filter = nil;
    switch (filterType) {
        case ImageFilterRotate: {
            filter = [[FLRotate alloc] init];
            break;
        }
        case ImageFilterMirror: {
            filter = [[FLMirror alloc] init];
            break;
        }
        case ImageFilterHalfMirror: {
            filter = [[FLMagicalMirror alloc] init];
            break;
        }
        case ImageFilterInvertColors: {
            filter = [[FLInvertColors alloc] init];
            break;
        }
        default: {
            filter = [[FLInvert alloc] init];
            break;
        }
    }
    
    retImage = [filter filteredImage:sourceImage];
    return retImage;
}

@end
