//
//  ImageProcessor.h
//  ImageTest
//
//  Created by Sergey Kim on 02.07.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Defines.h"
@import UIKit;


@protocol ImageProcessingDelegate <NSObject>

- (void) didConvertImageAtIndex:(NSUInteger)index;
@optional

- (void) didStepProgress:(float)progress forIndex:(NSUInteger)index;

@end


@interface ImageProcessor : NSObject

@property (strong) NSMutableArray * images;
@property (weak) id<ImageProcessingDelegate> delegate;

- (void) addImage:(UIImage*)sourceImage filter:(ImageFilterType)imageFilter slowProcessing:(BOOL)processing;
- (void) removeItemAtIndex:(NSUInteger)index;

@end
