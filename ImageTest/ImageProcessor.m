//
//  ImageProcessor.m
//  ImageTest
//
//  Created by Sergey Kim on 02.07.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import "ImageProcessor.h"
#import "ImageData.h"
#import "ImageFilter.h"

@interface ImageProcessor()

@property (strong) ImageFilter * filter;

@end

@implementation ImageProcessor

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *storedData = [currentDefaults objectForKey:@"storedImageItems"];
    if ( storedData != nil )
    {
        NSArray *oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:storedData];
        if (oldSavedArray != nil)
            _images = [[NSMutableArray alloc] initWithArray:oldSavedArray];
        else
            _images = [[NSMutableArray alloc] init];
    }
    else {
        _images = [NSMutableArray new];
    }
    
    _filter = [[ImageFilter alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveData) name:@"WillTerminate" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveData) name:@"EnterBackground" object:nil];
    return self;
}

- (void) addImage:(UIImage*)sourceImage filter:(ImageFilterType)imageFilter slowProcessing:(BOOL)processing {
    ImageData * iData = [[ImageData alloc] init];
    iData.image = [UIImage imageNamed:@"placeholder"];
    iData.identifier = [[NSUUID UUID] UUIDString];
    iData.progress = 0;
    [_images addObject:iData];
    
    NSUInteger index = [_images count]-1;
    NSUInteger tickCount = arc4random() % 10 + 6;
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if ( processing ) {
            [self runLongLoopWithIndex:index imageData:iData count:tickCount];
        }
        
        UIImage * filterImage = [_filter filteredImageFrom:sourceImage withType:imageFilter];
        iData.progress = 1;
        iData.image = filterImage;
        
        dispatch_async( dispatch_get_main_queue(), ^{
            if ( [_delegate respondsToSelector:@selector(didConvertImageAtIndex:)] ) {
                [_delegate didConvertImageAtIndex:index];
            }
        });
    });
}

- (void) runLongLoopWithIndex:(NSUInteger)index imageData:(ImageData*)iData count:(NSUInteger)tickCount {
    float step = 1.0 / (float)tickCount;

    for ( int tick = 0; tick < tickCount; tick++ ) {
        iData.progress = step*tick;
        dispatch_async( dispatch_get_main_queue(), ^{
            if ( [_delegate respondsToSelector:@selector(didStepProgress:forIndex:)] ) {
                [_delegate didStepProgress:step*tick forIndex:index];
            }
        });
        sleep(1);
    }
}

- (void) removeItemAtIndex:(NSUInteger)index {
    [_images removeObjectAtIndex:index];
}

- (void) saveData {
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSArray *copyArray = [[NSArray alloc] initWithArray:_images];
        [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:copyArray] forKey:@"storedImageItems"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    });
}

@end
