//
//  ProgressView.h
//  ImageTest
//
//  Created by Sergey Kim on 02.07.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgressView : UIView

- (void) startAnimating;
- (void) stopAnimating;
- (void) setProgress:(float)value;

@end
