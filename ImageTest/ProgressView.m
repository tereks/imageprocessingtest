//
//  ProgressView.m
//  ImageTest
//
//  Created by Sergey Kim on 02.07.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import "ProgressView.h"

@interface ProgressView ()

@property (weak, nonatomic) IBOutlet UIProgressView *progressIndicator;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityView;

@end

@implementation ProgressView

- (void) startAnimating {
    [_activityView startAnimating];
    [_progressIndicator setProgress:0];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 1;
    }];
}

- (void) stopAnimating {
    [_activityView stopAnimating];
    [_progressIndicator setProgress:1];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0;
    }];
}

- (void) setProgress:(float)value {
    [_progressIndicator setProgress:value];
}

@end
