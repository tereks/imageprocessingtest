//
//  ImageViewCell.h
//  ImageTest
//
//  Created by Sergey Kim on 02.07.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *m_imageView;
@property (weak, nonatomic) IBOutlet UIProgressView *m_progressView;

- (void) setProgress:(float)value;

@end
