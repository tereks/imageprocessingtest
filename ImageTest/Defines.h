//
//  Defines.h
//  ImageTest
//
//  Created by Sergey Kim on 03.08.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#ifndef ImageTest_Defines_h
#define ImageTest_Defines_h

typedef NS_ENUM(NSInteger, ImageFilterType) {
    ImageFilterRotate           = 0,
    ImageFilterBlackAndWhite    = 1,
    ImageFilterMirror           = 2,
    ImageFilterInvertColors     = 3,
    ImageFilterHalfMirror       = 4,
};

#endif
