//
//  ImageData.m
//  ImageTest
//
//  Created by Sergey Kim on 02.07.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import "ImageData.h"

@implementation ImageData

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self) {
        self.image = [coder decodeObjectForKey:@"ID_Image"];
        self.identifier = [coder decodeObjectForKey:@"ID_Identifier"];
        self.progress = [coder decodeFloatForKey:@"ID_Progress"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    
    [coder encodeObject:self.image forKey:@"ID_Image"];
    [coder encodeObject:self.identifier forKey:@"ID_Identifier"];
    [coder encodeFloat:self.progress forKey:@"ID_Progress"];
}

@end
