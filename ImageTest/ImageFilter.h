//
//  ImageFilter.h
//  ImageTest
//
//  Created by Sergey Kim on 03.08.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Defines.h"
@import UIKit;

@interface ImageFilter : NSObject

- (UIImage*) filteredImageFrom:(UIImage*)sourceImage withType:(ImageFilterType)filterType;

@end
