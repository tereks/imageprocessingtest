//
//  FLMirror.m
//  ImageTest
//
//  Created by Sergey Kim on 03.08.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import "FLMirror.h"

@implementation FLMirror

- (UIImage*) filteredImage:(UIImage*)sourceImage {
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef mainViewContentContext = CGBitmapContextCreate (NULL, sourceImage.size.width, sourceImage.size.height, 8, 0, colorSpace,
                                                                 (kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst));
    CGColorSpaceRelease(colorSpace);
    
    CGContextTranslateCTM(mainViewContentContext, 0.0, sourceImage.size.height);
    CGContextScaleCTM(mainViewContentContext, 1.0, -1.0);
    
    CGContextDrawImage(mainViewContentContext, CGRectMake(0, 0, sourceImage.size.width, sourceImage.size.height ), sourceImage.CGImage);
    
    CGImageRef reflectionImage = CGBitmapContextCreateImage(mainViewContentContext);
    CGContextRelease(mainViewContentContext);
    
    UIImage *theImage = [UIImage imageWithCGImage:reflectionImage];
    
    CGImageRelease(reflectionImage);
    
    return theImage;
}

@end
