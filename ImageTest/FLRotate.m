//
//  FLRotate.m
//  ImageTest
//
//  Created by Sergey Kim on 03.08.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import "FLRotate.h"

@implementation FLRotate

- (UIImage*) filteredImage:(UIImage*)sourceImage {
    CGSize size = sourceImage.size;
    UIGraphicsBeginImageContext(CGSizeMake(size.height, size.width));
    [[UIImage imageWithCGImage:[sourceImage CGImage] scale:1.0 orientation:UIImageOrientationRight] drawInRect:CGRectMake(0,0,size.height ,size.width)];
    UIImage* editImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return editImage;
}

@end
