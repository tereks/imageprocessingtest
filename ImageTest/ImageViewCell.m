//
//  ImageViewCell.m
//  ImageTest
//
//  Created by Sergey Kim on 02.07.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import "ImageViewCell.h"

@implementation ImageViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void) setProgress:(float)value {
    [_m_progressView setProgress:value];
    
    if ( value == 0 || value == 1 ) {
        _m_progressView.hidden = YES;
    }
    else {
        _m_progressView.hidden = NO;
    }
}

@end
