//
//  ImageDownloader.h
//  ImageTest
//
//  Created by Sergey Kim on 02.07.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageDownloader : NSObject

+ (id) instance;
- (void) startDownloadImageWithURL:(NSURL*)url
                          progress:(void (^)(NSUInteger bytes, long long totalBytes, long long totalBytesExpected))progressBlock
                           success:(void (^)(id result))successBlock
                             error:(void (^)(NSError* error))errorBlock;

@end
