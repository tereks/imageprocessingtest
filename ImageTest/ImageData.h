//
//  ImageData.h
//  ImageTest
//
//  Created by Sergey Kim on 02.07.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

@interface ImageData : NSObject

@property (strong) UIImage * image;
@property (strong) NSString * identifier;
@property (assign) float progress;

@end
